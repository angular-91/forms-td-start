import {Component, OnInit, ViewChild} from '@angular/core';
import {FormArray, FormControl, FormGroup, NgForm, Validators} from "@angular/forms";
import {Observable} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  /*@ViewChild('formSignup') signupForm: NgForm;
  defaultValue= 'pet';
  answer = "";
  gender = ['male', 'female', 'other']
  user = {
    name: '',
    email: '',
    secret: '',
    answer: '',
    gender: ''
  }
  submitted =  false;

  suggestUserName() {
    const suggestedName = 'Superuser';
    // this.signupForm.setValue({
    //   userData: {
    //     username: suggestedName,
    //     email: 'a@a.com'
    //   },
    //   secret: 'teacher',
    //   questionAnswer: '',
    //   gender: 'female'
    // })
    this.signupForm.form.patchValue({userData:{
          username: suggestedName
      }})
  }

  //check available validators- https://angular.io/api/forms/Validators

  onSubmit(form: NgForm){
    // console.log(form)
    console.log(this.signupForm)
    this.submitted = true;
    this.user.name = this.signupForm.value.userData.username
    this.user.email = this.signupForm.value.userData.email
    this.user.secret = this.signupForm.value.secret
    this.user.answer = this.signupForm.value.questionAnswer
    this.user.gender = this.signupForm.value.gender

    this.signupForm.reset();
  }*/

  // Reactive approach of handling forms

  genders = ['male', 'female', 'other']
  signupForm: FormGroup
  forbiddenUserNames = ['pooj', 'akku']

  ngOnInit() {
    this.signupForm = new FormGroup({
      'userData': new FormGroup({
      'username' : new FormControl(null, [Validators.required, this.forbiddenNames.bind(this)]),
      'email' : new FormControl(null, [Validators.required, Validators.email], this.forbiddenEmails),
      }),
      'gender' : new FormControl('female'),
      'hobbies' : new FormArray([])
    })

    // this.signupForm.valueChanges.subscribe((value)=>{
    //   console.log("value Changes:" , value);
    // })

    this.signupForm.statusChanges.subscribe((value)=>{
      console.log("value Changes:" , value);
    })

    this.signupForm.setValue({
      userData: {
        username: "Pooja",
        email: "jai@gmail.com"
      },
      gender: 'female',
      hobbies: []
    })

    this.signupForm.patchValue({
      gender : 'male'
    })


  }

  onSubmit(){
    console.log(this.signupForm)
    this.signupForm.reset()
  }

  onAddHobby(){
    const control =  new FormControl(null, Validators.required);
    (<FormArray>this.signupForm.get('hobbies')).push(control);
  }

  getControls(){
    return (<FormArray>this.signupForm.get('hobbies')).controls;
  }


  forbiddenNames(control: FormControl): {[s: string]: boolean}{
    if(this.forbiddenUserNames.indexOf(control.value) !== -1){
      return {'nameIsForbidden' : true}
    }
    return null;
  }

  //async validator

  forbiddenEmails(control: FormControl): Promise<any> | Observable<any> {
    const promise = new Promise<any>((resolve, reject) => {
      setTimeout(()=>{
        if(control.value ===  'test@test.com'){
          resolve({'emailIsForbidden': true});
        } else{
          resolve (null)
        }
      }, 1500)
    } );
    return promise;

  }

}
